from fastapi import FastAPI
import oauth2 as oauth

app = FastAPI()  # uvicorn main:app --reload


@app.post("/lti")
async def handle_lti(lti_data: dict):
    params = lti_data.get('params')
    consumer_secret = 'YOUR_SECRET_KEY'  # your consumer secret

    token = oauth.Token(key='',
                        secret='')
    consumer = oauth.Consumer(key=params.get('oauth_consumer_key'),
                              secret=consumer_secret)

    # select signature method
    signature_method = oauth.SignatureMethod_HMAC_SHA1()

    # generete signature
    request = oauth.Request(method=lti_data.get('method'),
                            url=lti_data.get('action'),
                            parameters=params)
    request.sign_request(signature_method,
                         consumer,
                         token)

    # performs signature validation
    signature = str.encode(params.get('oauth_signature'))
    valid = signature_method.check(request=request,
                                   consumer=consumer,
                                   token=token,
                                   signature=signature)

    if not valid:
        return {"Invalid LTI request signature."}

    # @TODO With a valid signature, the edtech must now log
    # the user into its solution and redirect them to the platform.

    return {"LTI request processed successfully!"}
